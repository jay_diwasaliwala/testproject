﻿using System.Data;

namespace TestWebAPI3.Service
{
    public interface IEmp
    {
        public DataSet GetData();
    }
}
