﻿using System.Data;
using TestWebAPI3.Repository;

namespace TestWebAPI3.Service
{
    public class Emp:IEmp
    {
        private readonly DbContext _db;
        public Emp(DbContext dbContext) { 
        _db = dbContext;
        }
        public DataSet GetData() {
            return _db.GetDataSet();
            
        }
    }
}
