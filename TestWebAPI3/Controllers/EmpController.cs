﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TestWebAPI3.Service;

namespace TestWebAPI3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmpController : ControllerBase
    {
        private readonly IEmp _emp;
        public EmpController(IEmp emp)
        {
            _emp = emp;
        }
        [HttpGet]
        public ActionResult Get()
        {
            return Ok(_emp.GetData());
        }
    }
}
