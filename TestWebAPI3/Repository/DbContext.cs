﻿using System.Data;
using System.Data.SqlClient;

namespace TestWebAPI3.Repository
{
    public class DbContext
    {
        SqlConnection sqlConnection = new SqlConnection(Utility.SqlConnStr.SqlConnString);

        public DataSet GetDataSet()
        {
            sqlConnection.Open();
            DataSet dataSet = new DataSet();
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = sqlConnection;
            sqlCommand.CommandType = CommandType.Text;
            sqlCommand.CommandText = "select * from Employee";

            SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand);
            adapter.Fill(dataSet);
            sqlConnection.Close();
            return dataSet;
        }
    }
}
